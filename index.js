
// console.log("Hi");
//3. Declare 3 global variables without initialization called
//username,password and role.
let username;
let password;
let role;
//4. Create a login function which is able to prompt the user to provide their username, password and role.
//Add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
function login(){
//USERNAME
	username = prompt("Enter your Username:");
	if(username === null || username === ""){
		alert("Username should not be empty")
	}
//PASSWORD
	password = prompt("Enter your Password:");
	if(password === null || password === ""){
		alert("Pssword should not be empty")
	}
//ROLE
	role = prompt("Enter your role:");
	if(role === null || role === ""){
		alert("Role should not be empty")
		if(role === "admin"){
			console.log("Welcome back to the class portal, admin!");
		}
		else if(role === "teacher"){
			console.log("Thank you for logging in, teacher!");
		}
		else if(role === "student"){
			console.log("Welcome to the class portal, student!");
		}
		else{
			console.log("Role out of range.");
		}
	}
	else{
		if(role === "admin"){
			console.log("Welcome back to the class portal, admin!");
		}
		else if(role === "teacher"){
			console.log("Thank you for logging in, teacher!");
		}
		else if(role === "student"){
			console.log("Welcome to the class portal, student!");
		}
		else{
			console.log("Role out of range.");
		}
	}
	
}
login();

//Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for the user about their letter equivalent in the console.

let grade1 = parseInt(prompt("Enter your first grade: "));
let grade2 = parseInt(prompt("Enter your second grade: "));
let grade3 = parseInt(prompt("Enter your third grade: "));
let grade4 = parseInt(prompt("Enter your last grade: "));

function checkAverage(grade1, grade2, grade3,grade4) {
	let average = (grade1 + grade2 + grade3 + grade4) / 4;
	average = Math.round(average);

	if (average <= 74){
		console.log("Hello, student, your average is " + average +" The letter equivalent is F");
	} else if (average >= 75 && average <= 79) {
		console.log("Hello, student, your average is " + average +" The letter equivalent is D");
	} else if (average >= 80 && average <= 84) {
		console.log("Hello, student, your average is " + average +"The letter equivalent is C");
	} else if (average >= 85 && average <= 89) {
		console.log("Hello, student, your average is " + average +" The letter equivalent is B");
	} else if (average >= 90 && average <= 95) {
		console.log("Hello, student, your average is " + average +" The letter equivalent is A");
	}else if (average >= 96) {
		console.log("Hello, student, your average is " + average +" The letter equivalent is A+");
	}
};

checkAverage(71,70,73,74)
checkAverage(75,75,76,78)
checkAverage(80,81,82,78)
checkAverage(84,85,87,88)
checkAverage(89,90,91,90)
checkAverage(91,96,97,95)
